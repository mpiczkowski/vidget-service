# VidGet service is a simple Spring REST application for getting video metadata


> There are only two hard things in Computer Science: 
> cache invalidation and naming things.
> 
> -- Phil Karlton
>

So I named the service "VidGet" which sounds similar to "widget" and also has very simple features.

It allows uploading video files of max size = 25 MB and querying for video properties such as:
 
 * duration
 * video size
 * video bit rate
 * video codec
 * audio bit rate
 * audio codec

# API

Below are example Curl requests:

## Upload video

```
curl -F 'file=@/tmp/my_video_file.mp4 localhost:8080/v1/videos
```

Response: 

```
{"id":"9729d3a4-80e5-4337-a251-306065f53418"}
```

The response `id` is a unique identifier to be used in metadata query

## Query video metadata

```
curl localhost:8080/v1/videos/{video ID}
```

e.g.: 

```
curl localhost:8080/v1/videos/9729d3a4-80e5-4337-a251-306065f53418
```

As it is possible to have multiple video and audio streams in the same video file then
the result of query has a form of audio and video lists, e.g.:


```
{
   "id":"90337e39-4185-41ba-b9ef-2547cb5c141f",
   "videos":[
      {
         "index":0,
         "codecName":"h264",
         "duration":5.28,
         "bitRate":1205959
      }
   ],
   "audios":[
      {
         "index":1,
         "codecName":"aac",
         "duration":5.312,
         "bitRate":384828
      }
   ],
   "duration":5.312,
   "size":1055728
}
```

In cases then there is an error, e.g. the endpoint does not exist, the response will have a meaningful status and 
JSON error message, e.g.:

Request: 

```
curl -i localhost:8080/v1/wrongurl

```

Response:

```
HTTP/1.1 404 
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Sat, 22 Sep 2018 19:28:20 GMT

{"timestamp":"2018-09-22T19:28:20.031+0000","status":404,"error":"Not Found","message":"No message available","path":"/v1/wrongurl"}

```
# Internals

When files are uploaded the application does simple validation of file content.
It must not be empty and must have a correct file extension (specified in `application.properties` property `video.allowedExtensions`)
An alternative would be to use ffmpeg to check if file has video format, but this would impact the api performance.
In worst case if the file cannot be processed the metadata will not be created.

Currently the files are saved in local file system (folder specified in `application.properties` property `storage.dir`).
This is not a production ready storage, as e.g. if you considered load-balanced multiple application nodes, 
then they will not be able to see neighbour nodes' storage unless they are using the same mapped volume for storing the file.

The name of saved binary file is the same as video ID.

When you upload a video it responds immediately after the file is stored. Metadata calculation is done asynchronously
and saved in separate json file next to the binary video file.
Therefore it may happen that if you try to query for metadata it won't be available immediately after the upload
and you get response status code `102 Processing` (from [WebDAV specification RFC 2518](https://tools.ietf.org/html/rfc2518#section-10.1) 

The name of the metadata file is the same as video ID plus `.json` extension.


# Build

```
mvn clean package
```

You need to have FFMPEG installed on server where tests are running. Otherwise some tests will fail.

You can still build project without tests with: 

```
mvn clean package -DskipTests
```


# Run

The application requires FFMPEG installed on the system where it's running.
You can either install FFMPEG on your development machine or run the application in provided [Dockerfile](docker/Dockerfile).

## Without Docker

```
java -jar target/vidget-service-0.0.1-SNAPSHOT.jar
```

## With Docker

```
./docker/dev-build.sh
./docker/dev-run.sh
```

# TODOs:
1. Uploaded files remain on disk after integration tests. If you are running tests on volatile continues integration machine, 
which is later destroyed then this is not a problem, otherwise you can garbage the test server (and your local dev machine).
There is a dedicated TODO in the code to add a cleanup.

2. When processing of the video for metadata fails there is no retry (another TODO in the source code)

3. Self-documenting API: consider Hipermedia-Driven REST API, e.g. like [this](https://github.com/opencredo/spring-hateoas-sample) or
add Swagger docs.

4. Consider refactoring packages into maven modules separate for ports and adapters like [here](https://github.com/gshaw-pivotal/spring-hexagonal-example). As this is a very simple app, it was initially done in single module.

