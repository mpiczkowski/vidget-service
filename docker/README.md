# Minimal Java 8 + FFMPEG Docker image built on Alpine Linux

FFMPEG version: 

```

ffmpeg version 4.0 Copyright (c) 2000-2018 the FFmpeg developers
  built with gcc 5.3.0 (Alpine 5.3.0)
  configuration: 
    --enable-version3 
    --enable-gpl 
    --enable-nonfree 
    --enable-small 
    --enable-libmp3lame 
    --enable-libx264 
    --enable-libx265 
    --enable-libvpx 
    --enable-libtheora 
    --enable-libvorbis 
    --enable-libopus 
    --enable-libass 
    --enable-libwebp 
    --enable-librtmp 
    --enable-postproc 
    --enable-avresample 
    --enable-libfreetype 
    --enable-openssl 
    --disable-debug
  libavutil      56. 14.100 / 56. 14.100
  libavcodec     58. 18.100 / 58. 18.100
  libavformat    58. 12.100 / 58. 12.100
  libavdevice    58.  3.100 / 58.  3.100
  libavfilter     7. 16.100 /  7. 16.100
  libavresample   4.  0.  0 /  4.  0.  0
  libswscale      5.  1.100 /  5.  1.100
  libswresample   3.  1.100 /  3.  1.100
  libpostproc    55.  1.100 / 55.  1.100

```

## Build

```
docker build -t vidget/ffmpeg .
```

## Run

```
 docker run --name ffmpeg -d  vidget/ffmpeg
```
This will run container named `ffmpeg` and print container id

You can play with FFMPEG in the container like this:

 * Copy a video from docker host into the container

```
docker cp <video path> <container id>:/tmp
```

e.g.:
 
```
 docker cp ~/video.mp4 0f94a3e58d5d2c:/tmp
```

 * ssh to the container

```
docker exec -it <container id> sh
```

Then you can display movie metadata like this: 

```
ffprobe -show_streams -show_format /tmp/capture.mp4 
```

It will print information like: 

```
Input #0, mov,mp4,m4a,3gp,3g2,mj2, from '/tmp/capture.mp4':
  Metadata:
    major_brand     : isom
    minor_version   : 0
    compatible_brands: mp41avc1
    creation_time   : 2018-01-05T12:39:49.000000Z
    encoder         : vlc 2.2.2 stream output
    encoder-eng     : vlc 2.2.2 stream output
  Duration: 00:07:13.72, start: 0.000000, bitrate: 585 kb/s
    Stream #0:0(eng): Video: h264 (avc1 / 0x31637661), yuv420p, 3840x1080 [SAR 1:1 DAR 32:9], 585 kb/s, 6.82 fps, 9.33 tbr, 1000k tbn, 14 tbc (default)
    Metadata:
      creation_time   : 2018-01-05T12:39:49.000000Z
      handler_name    : VideoHandler
[STREAM]
index=0
codec_name=h264
codec_long_name=unknown
profile=100
codec_type=video
codec_time_base=15917731/217120849
codec_tag_string=avc1
codec_tag=0x31637661
width=3840
height=1080
coded_width=3840
coded_height=1088
has_b_frames=10
sample_aspect_ratio=1:1
display_aspect_ratio=32:9
pix_fmt=yuv420p
level=50
color_range=unknown
color_space=unknown
color_transfer=unknown
color_primaries=unknown
chroma_location=left
field_order=unknown
timecode=N/A
refs=1
is_avc=true
nal_length_size=4
id=N/A
r_frame_rate=28/3
avg_frame_rate=2089516604/306376503
time_base=1/1000000
start_pts=0
start_time=0.000000
duration_ts=433718344
duration=433.718344
bit_rate=585073
max_bit_rate=N/A
bits_per_raw_sample=8
nb_frames=2958
nb_read_frames=N/A
nb_read_packets=N/A
DISPOSITION:default=1
DISPOSITION:dub=0
DISPOSITION:original=0
DISPOSITION:comment=0
DISPOSITION:lyrics=0
DISPOSITION:karaoke=0
DISPOSITION:forced=0
DISPOSITION:hearing_impaired=0
DISPOSITION:visual_impaired=0
DISPOSITION:clean_effects=0
DISPOSITION:attached_pic=0
DISPOSITION:timed_thumbnails=0
TAG:creation_time=2018-01-05T12:39:49.000000Z
TAG:language=eng
TAG:handler_name=VideoHandler
[/STREAM]
[FORMAT]
filename=/tmp/capture.mp4
nb_streams=1
nb_programs=0
format_name=mov,mp4,m4a,3gp,3g2,mj2
format_long_name=unknown
start_time=0.000000
duration=433.718344
size=31762952
bit_rate=585872
probe_score=100
TAG:major_brand=isom
TAG:minor_version=0
TAG:compatible_brands=mp41avc1
TAG:creation_time=2018-01-05T12:39:49.000000Z
TAG:encoder=vlc 2.2.2 stream output
TAG:encoder-eng=vlc 2.2.2 stream output
[/FORMAT]

```



Example usages: 
```
 ffprobe -v error -show_entries stream=width,height,bit_rate,duration -print_format json /tmp/capture.mp4 
 ffprobe -v error -show_streams -show_format -print_format json /tmp/capture.mp4 
```