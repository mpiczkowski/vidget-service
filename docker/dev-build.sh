#!/bin/bash -e

VERSION="0.0.1-SNAPSHOT"
IMAGE="vidget/service"
TAG="latest"

cd $(dirname ${BASH_SOURCE[0]})

# Build base image (mostly useful on build server with volatile nodes, as docker cache is used on your dev machine)
docker build -f Dockerfile_base -t vidget/base .

pushd ..

# Build app
mvn -DskipTests clean package

# Copy jar to docker context folder
cp target/vidget-service-${VERSION}.jar docker

popd

# Build app docker
docker build -f Dockerfile -t ${IMAGE}:${TAG} .

# Cleanup
rm vidget-service-${VERSION}.jar





