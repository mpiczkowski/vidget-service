#!/bin/bash

NAME="vidget-service"
IMAGE="vidget/service"
TAG="latest"
set -x

# stop and destroy if already running
docker stop ${NAME} > /dev/null 2>&1
docker rm ${NAME} > /dev/null 2>&1

# run in the foreground and remove the container after we're done
docker run --rm=true -it \
    -p 8080:8080 \
    --name ${NAME} \
    ${IMAGE}:${TAG} \
    ${@}
