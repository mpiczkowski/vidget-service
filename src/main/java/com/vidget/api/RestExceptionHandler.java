package com.vidget.api;


import com.vidget.api.dto.ErrorResponse;
import com.vidget.api.validation.exceptions.InvalidFileTypeException;
import com.vidget.api.validation.exceptions.NoContentException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NoContentException.class)
    protected ResponseEntity<Object> handleNoContentException(NoContentException ex) {
        return buildResponseEntity(new ErrorResponse(BAD_REQUEST, ex));
    }

    @ExceptionHandler(InvalidFileTypeException.class)
    protected ResponseEntity<Object> handleInvalidFileType(InvalidFileTypeException ex) {
        return buildResponseEntity(new ErrorResponse(BAD_REQUEST, ex));
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    protected ResponseEntity<Object> handleFileTooLarge(MaxUploadSizeExceededException ex) {
        return buildResponseEntity(new ErrorResponse(BAD_REQUEST, "Maximum upload size exceeded"));
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, @Nullable Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute("javax.servlet.error.exception", ex, 0);
        }

        return buildResponseEntity(new ErrorResponse(status, ex));
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAll(final Exception ex, final WebRequest request) {
        return buildResponseEntity(new ErrorResponse(INTERNAL_SERVER_ERROR, ex));
    }

    private ResponseEntity<Object> buildResponseEntity(ErrorResponse errorResponse) {
        return new ResponseEntity<>(errorResponse, errorResponse.getStatus());
    }
}
