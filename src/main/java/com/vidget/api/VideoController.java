package com.vidget.api;

import lombok.AllArgsConstructor;
import static org.springframework.http.HttpStatus.CREATED;

import com.vidget.api.dto.VideoAddResponse;
import com.vidget.api.validation.FileUploadValidator;
import com.vidget.domain.persistence.exceptions.DataStoreException;
import com.vidget.domain.services.VideoStoreService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@RestController
@AllArgsConstructor
public class VideoController {
    private final VideoStoreService videoStoreService;
    private final FileUploadValidator fileValidator;

    @GetMapping(value = "/v1/videos/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getVideo(@PathVariable UUID id) throws DataStoreException {
        return videoStoreService.findById(id)
                .map(video -> new ResponseEntity(video, HttpStatus.OK))
                .orElseGet(() -> getNotFoundResponse(id));
    }

    @PostMapping(value = "/v1/videos", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(CREATED)
    public VideoAddResponse upload(MultipartFile file) throws IOException, DataStoreException {
        fileValidator.validate(file);
        UUID videoId = videoStoreService.save(file.getBytes());
        return new VideoAddResponse(videoId);
    }

    private ResponseEntity getNotFoundResponse(UUID id) {
        return videoStoreService.binaryExists(id)
                ? new ResponseEntity(HttpStatus.PROCESSING)// video was uploaded and not processed
                : new ResponseEntity(HttpStatus.NOT_FOUND);// video not found
    }
}

