package com.vidget.api.dto;

import lombok.Value;

import java.util.UUID;

@Value
public class VideoAddResponse {
    private final UUID id;
}
