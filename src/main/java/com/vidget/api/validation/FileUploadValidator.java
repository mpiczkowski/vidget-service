package com.vidget.api.validation;

import com.vidget.api.validation.exceptions.InvalidFileTypeException;
import com.vidget.api.validation.exceptions.NoContentException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class FileUploadValidator {

    private final List<String> allowedExtensions;

    public FileUploadValidator(@Value("${video.allowedExtensions}") String allowedExtensionsProp) {
        this.allowedExtensions = Arrays.stream(allowedExtensionsProp.split(","))
                .map(String::trim)
                .filter(txt -> !txt.isEmpty())
                .collect(Collectors.toList());
    }

    public void validate(MultipartFile multipartFile) {
        validateContentData(multipartFile);
        validateContentType(multipartFile.getOriginalFilename());
    }

    /**
     * If there is no data then fail the upload.
     *
     * @param multipartFile an uploaded file
     */
    private void validateContentData(MultipartFile multipartFile) {
        if (multipartFile == null || multipartFile.isEmpty()) {
            throw new NoContentException();
        }
    }

    /**
     * If file has extension and extension is allowed then pass check,
     * otherwise fail.
     *
     * @param fileName a name of uploaded file
     */
    private void validateContentType(String fileName) {
        getExtension(fileName)
                .filter(allowedExtensions::contains)
                .orElseThrow(InvalidFileTypeException::new);
    }

    private Optional<String> getExtension(String fileName) {
        int indx = fileName.lastIndexOf('.');
        return indx > 0 ? Optional.of(fileName.substring(indx + 1)) : Optional.empty();
    }
}
