package com.vidget.api.validation.exceptions;

public class InvalidFileTypeException extends RuntimeException {
    public InvalidFileTypeException() {
        super("File is not a video");
    }
}
