package com.vidget.api.validation.exceptions;

public class NoContentException extends RuntimeException {
    public NoContentException(){
        super("File is empty");
    }
}
