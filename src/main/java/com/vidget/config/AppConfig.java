package com.vidget.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;

import java.util.concurrent.Executors;

@Configuration
public class AppConfig {

    /**
     * Make all spring events asynchronous.
     * If you need to support both synchronous and asynchronous events check:
     * <a href="https://www.keyup.eu/en/blog/101-synchronous-and-asynchronous-spring-events-in-one-application"> this</a>
     */
    @Bean
    public ApplicationEventMulticaster applicationEventMulticaster() {
        SimpleApplicationEventMulticaster eventMulticaster = new SimpleApplicationEventMulticaster();
        eventMulticaster.setTaskExecutor(Executors.newFixedThreadPool(5));
        return eventMulticaster;
    }

}
