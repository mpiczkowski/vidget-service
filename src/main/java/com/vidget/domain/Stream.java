package com.vidget.domain;

import lombok.Builder;
import lombok.Value;

/**
 * Video stream metadata. This domain class has support for JSON serialization.
 */
@Builder
@Value
public class Stream {

    private final int index;
    private final String codecName;
    private final double duration;
    private final long bitRate;
}
