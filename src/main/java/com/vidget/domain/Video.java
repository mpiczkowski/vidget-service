package com.vidget.domain;

import java.util.List;
import java.util.UUID;

import lombok.Builder;
import lombok.Value;

/**
 * Video metadata. This domain class has support for JSON serialization.
 */
@Value
@Builder
public class Video {

    private final UUID id;
    /**
     * Duration in seconds
     */
    private final double duration;

    /**
     * File size in bytes
     */
    private final long size;

    private final List<Stream> videos;
    private final List<Stream> audios;
}
