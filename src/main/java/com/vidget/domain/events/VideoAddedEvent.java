package com.vidget.domain.events;

import lombok.Value;

import java.util.UUID;

@Value
public class VideoAddedEvent {
    private UUID videoId;
}
