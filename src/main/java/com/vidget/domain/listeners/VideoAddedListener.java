package com.vidget.domain.listeners;

import java.util.UUID;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.vidget.domain.events.VideoAddedEvent;
import com.vidget.domain.services.VideoMetadataService;
import com.vidget.domain.services.exceptions.MetadataProcessingException;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@AllArgsConstructor
public class VideoAddedListener {

    private final VideoMetadataService videoMetadataService;

    @EventListener
    public void handleEvent(VideoAddedEvent event) {
        UUID videoId = event.getVideoId();
        log.info("Video added, id: {}", videoId);
        try {
            videoMetadataService.process(videoId);
        } catch (MetadataProcessingException e) {
            log.error(String.format("Cannot process video %s for metadata extraction", videoId), e);
            //TODO: retry, notify admin or sth else..
        }
    }
}
