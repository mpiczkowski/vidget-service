package com.vidget.domain.persistence;

import static java.nio.file.Files.deleteIfExists;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vidget.domain.Video;
import com.vidget.domain.persistence.exceptions.DataAlreadyExistsException;
import com.vidget.domain.persistence.exceptions.DataStoreException;

import lombok.extern.slf4j.Slf4j;

/**
 * File-system based video repository
 */
@Slf4j
@Repository
public class FSVideoRepository implements VideoRepository {

    private static final String META_FILE_POSTFIX = ".json";

    private final Path storageRoot;
    private final ObjectMapper objectMapper;

    public FSVideoRepository(@Value("${storage.dir}") String rootPath, ObjectMapper objectMapper) throws IOException {
        this.storageRoot = Paths.get(rootPath);
        this.objectMapper = objectMapper;
        initializeStoreFolder();
    }

    @Override
    public String saveVideo(UUID videoId, byte[] content) throws DataStoreException {
        String fileName = videoId.toString();
        Path filePath = storageRoot.resolve(fileName);
        log.info("Storing file: {}", filePath);
        if (filePath.toFile().exists()) {
            throw new DataAlreadyExistsException(String.format("Video %s already exists", fileName));
        }
        try {
            Files.write(filePath, content);
        } catch (IOException e) {
            throw new DataStoreException(String.format("Could not store video %s", fileName), e);
        }
        return filePath.toString();
    }

    @Override
    public void saveMetaData(Video video) throws DataStoreException {
        try {
            Path metaFilePath = getBinaryPath(video.getId());
            deleteIfExists(metaFilePath);
            objectMapper.writeValue(metaFilePath.toFile(), video);
        } catch (IOException e) {
            throw new DataStoreException(String.format("Could not store video metadata for video id %s", video.getId()), e);
        }
    }

    @Override
    public Optional<Video> findById(UUID videoId) throws DataStoreException {
        Path metadataPath = getBinaryPath(videoId);
        return metadataPath.toFile().exists() ? toVideo(metadataPath) : Optional.empty();
    }

    @Override
    public String getVideoPath(UUID videoId) {
        return getBinaryPath(videoId).toString();
    }

    @Override
    public String getMetaDataPath(UUID videoId) {
        return storageRoot.resolve(videoId.toString()).toString();
    }

    private Optional<Video> toVideo(Path metadataPath) throws DataStoreException {
        try {
            return Optional.of(objectMapper.readValue(metadataPath.toFile(), Video.class));
        } catch (IOException e) {
            throw new DataStoreException(String.format("Cannot read metadata from file %s", metadataPath), e);
        }
    }

    private String getMetaFileName(UUID videoId) {
        return videoId.toString().concat(META_FILE_POSTFIX);
    }

    private Path getBinaryPath(UUID videoId) {
        return storageRoot.resolve(getMetaFileName(videoId));
    }

    private void initializeStoreFolder() throws IOException {
        Files.createDirectories(storageRoot);
    }

}
