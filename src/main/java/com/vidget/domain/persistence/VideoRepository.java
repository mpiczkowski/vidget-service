package com.vidget.domain.persistence;

import com.vidget.domain.Video;
import com.vidget.domain.persistence.exceptions.DataStoreException;

import java.util.Optional;
import java.util.UUID;

/**
 * Repository for storing video binary and metadata.
 */
public interface VideoRepository {
    String saveVideo(UUID videoId, byte[] content) throws DataStoreException;

    void saveMetaData(Video video) throws DataStoreException;

    Optional<Video> findById(UUID videoId) throws DataStoreException;

    String getVideoPath(UUID videoId);

    String getMetaDataPath(UUID videoId);
}
