package com.vidget.domain.persistence.exceptions;

public class DataAlreadyExistsException extends DataStoreException {
    public DataAlreadyExistsException(String message) {
        super(message);
    }
}
