package com.vidget.domain.persistence.exceptions;

public class DataStoreException extends Exception {
    public DataStoreException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataStoreException(String message) {
        super(message);
    }
}
