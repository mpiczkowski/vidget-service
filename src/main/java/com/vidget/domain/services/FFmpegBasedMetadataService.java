package com.vidget.domain.services;

import com.vidget.domain.Stream;
import com.vidget.domain.Video;
import com.vidget.domain.persistence.exceptions.DataStoreException;
import com.vidget.domain.services.exceptions.MetadataProcessingException;
import com.vidget.ffmpeg.probe.FFmpegFormat;
import com.vidget.ffmpeg.probe.FFmpegProbeResult;
import com.vidget.ffmpeg.probe.FFmpegStream;
import com.vidget.ffmpeg.probe.FFmpegStream.CodecType;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.groupingBy;
import static org.springframework.util.Assert.notNull;

@Slf4j
@Service
@AllArgsConstructor
public class FFmpegBasedMetadataService implements VideoMetadataService {
    private final FFmpegProcessor ffmpegProcessor;
    private final VideoStoreService videoStoreService;

    @Override
    public void process(UUID videoId) throws MetadataProcessingException {
        notNull(videoId, "Video id cannot be null");
        try {
            String videoFilePath = videoStoreService.getBinaryLocation(videoId);
            Video video = toVideo(videoId, getVideoAttributes(videoFilePath));
            videoStoreService.save(video);
        } catch (IOException exc) {
            throw new MetadataProcessingException(String.format("Cannot extract video metadata for %s", videoId), exc);
        } catch (DataStoreException exc) {
            throw new MetadataProcessingException(String.format("Cannot save video metadata for %s", videoId), exc);
        }

    }

    private Video toVideo(UUID videoId, FFmpegProbeResult ffmpegProbeResult) {
        Map<CodecType, List<FFmpegStream>> streamsPerType = ffmpegProbeResult.getStreams().stream()
                .collect(groupingBy(FFmpegStream::getCodecType));

        List<Stream> videoStreams = streamsPerType.getOrDefault(CodecType.VIDEO, emptyList()).stream()
                .map(this::toStream)
                .collect(Collectors.toList());

        List<Stream> audioStreams = streamsPerType.getOrDefault(CodecType.AUDIO, emptyList()).stream()
                .map(this::toStream)
                .collect(Collectors.toList());

        FFmpegFormat format = ffmpegProbeResult.getFormat();

        Video.VideoBuilder builder = Video.builder()
                .id(videoId)
                .videos(videoStreams)
                .audios(audioStreams);
        if (format != null) {
            builder
                    .duration(format.getDuration())
                    .size(format.getSize());
        }
        return builder.build();
    }

    private Stream toStream(FFmpegStream ffmpegStream) {
        return Stream.builder()
                .index(ffmpegStream.getIndex())
                .duration(ffmpegStream.getDuration())
                .bitRate(ffmpegStream.getBitRate())
                .codecName(ffmpegStream.getCodecName())
                .build();
    }


    private FFmpegProbeResult getVideoAttributes(String videoFilePath) throws IOException {
        return ffmpegProcessor.probe(videoFilePath);
    }
}
