package com.vidget.domain.services;

import com.vidget.ffmpeg.FFProbeWrapper;
import com.vidget.ffmpeg.probe.FFmpegProbeResult;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Processor running native ffprobe on a given video file.
 */
@Component
class FFmpegProcessor {

    FFmpegProbeResult probe(String videoPath) throws IOException {
        return FFProbeWrapper.run(videoPath);
    }
}
