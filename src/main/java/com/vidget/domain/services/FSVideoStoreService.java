package com.vidget.domain.services;

import com.vidget.domain.Video;
import com.vidget.domain.events.VideoAddedEvent;
import com.vidget.domain.persistence.VideoRepository;
import com.vidget.domain.persistence.exceptions.DataStoreException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.nio.file.Paths;
import java.util.Optional;
import java.util.UUID;

/**
 * Implementation of service which uses file system store.
 */
@Slf4j
@Service
public class FSVideoStoreService implements VideoStoreService {

    private final VideoRepository videoRepository;
    private final ApplicationEventPublisher eventPublisher;

    public FSVideoStoreService(VideoRepository videoRepository, ApplicationEventPublisher eventPublisher) {
        this.videoRepository = videoRepository;
        this.eventPublisher = eventPublisher;
    }

    @Override
    public UUID save(byte[] binaryData) throws DataStoreException {
        UUID videoId = UUID.randomUUID();
        videoRepository.saveVideo(videoId, binaryData);
        eventPublisher.publishEvent(new VideoAddedEvent(videoId));
        return videoId;
    }

    @Override
    public void save(Video video) throws DataStoreException {
        videoRepository.saveMetaData(video);
    }

    @Override
    public Optional<Video> findById(UUID id) throws DataStoreException {
        return videoRepository.findById(id);
    }

    @Override
    public boolean binaryExists(UUID id) {
        String videoPath = videoRepository.getMetaDataPath(id);
        return Paths.get(videoPath).toFile().exists();
    }

    @Override
    public String getBinaryLocation(UUID id) {
        return videoRepository.getMetaDataPath(id);
    }
}
