package com.vidget.domain.services;

import com.vidget.domain.services.exceptions.MetadataProcessingException;

import java.util.UUID;

/**
 * Manages video metadata processing
 */
public interface VideoMetadataService {
    void process(UUID videoId) throws MetadataProcessingException;
}
