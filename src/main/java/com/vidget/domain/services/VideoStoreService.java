package com.vidget.domain.services;

import com.vidget.domain.Video;
import com.vidget.domain.persistence.exceptions.DataStoreException;

import java.util.Optional;
import java.util.UUID;

/**
 * Manages videos metadata and binary data.
 * see also {@link FSVideoStoreService}
 */
public interface VideoStoreService {

    UUID save(byte[] binaryData) throws DataStoreException;

    void save(Video video) throws DataStoreException;

    Optional<Video> findById(UUID id) throws DataStoreException;

    boolean binaryExists(UUID id);

    String getBinaryLocation(UUID id);
}
