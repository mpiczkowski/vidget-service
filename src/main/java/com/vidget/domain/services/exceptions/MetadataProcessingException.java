package com.vidget.domain.services.exceptions;

public class MetadataProcessingException extends Exception {
    public MetadataProcessingException(String message, Throwable cause){
        super(message,cause);
    }
}
