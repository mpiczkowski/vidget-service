package com.vidget.ffmpeg;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.vidget.ffmpeg.probe.FFmpegProbeResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Util class which wraps native ffprobe process.
 *
 * It does not have any Spring dependencies.
 */
public class FFProbeWrapper {

    private static final List<String> probeArgs = Arrays.asList("ffprobe", "-v", "quiet", "-show_streams", "-show_format", "-print_format", "json");

    private FFProbeWrapper() {
    }

    /**
     * Run ffprobe on input video file
     *
     * @param videoPath an absolute path to video file
     * @return probe result
     * @throws IOException if an I/O error occurs
     */
    public static FFmpegProbeResult run(String videoPath) throws IOException {
        assertExists(videoPath);

        ImmutableList.Builder<String> argsBuilder = ImmutableList.builder();
        List<String> args = argsBuilder.addAll(probeArgs)
                .add(videoPath)
                .build();

        Process p = startProcess(args);

        BufferedReader br = bufferResult(p);

        exitOnTimeout(p);

        return readResult(br);
    }

    private static void assertExists(String videoPath) throws IOException {
        if (!Paths.get(videoPath).toFile().exists()) {
            throw new IOException(String.format("File does not exist %s", videoPath));
        }
    }

    private static Process startProcess(List<String> args) throws IOException {
        ProcessBuilder builder = new ProcessBuilder(args);
        builder.redirectErrorStream(true);
        return builder.start();
    }

    private static FFmpegProbeResult readResult(BufferedReader br) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS);
        return objectMapper.readValue(br, FFmpegProbeResult.class);
    }

    private static void exitOnTimeout(Process p) throws IOException {
        try {
            p.waitFor(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new IOException("Timed out waiting for to finish.");
        }
    }

    private static BufferedReader bufferResult(Process p) {
        return new BufferedReader(new InputStreamReader(p.getInputStream(), StandardCharsets.UTF_8));
    }


}
