package com.vidget.ffmpeg.probe;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.ToString;
import lombok.Value;

@Value
@Builder
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class FFmpegFormat {

    private final String filename;
    private final int nbStreams;
    private final String formatName;
    private final String formatLongName;
    private final double startTime;
    /**
     * Duration in seconds
     */
    private final double duration;

    /**
     * File size in bytes
     */
    private final long size;
    private final long bitRate;

    @JsonCreator
    public FFmpegFormat(@JsonProperty("filename") String filename,
                        @JsonProperty("nb_streams") int nbStreams,
                        @JsonProperty("format_name") String formatName,
                        @JsonProperty("format_long_name") String formatLongName,
                        @JsonProperty("start_time") double startTime,
                        @JsonProperty("duration") double duration,
                        @JsonProperty("size") long size,
                        @JsonProperty("bit_rate") long bitRate) {
        this.filename = filename;
        this.nbStreams = nbStreams;
        this.formatName = formatName;
        this.formatLongName = formatLongName;
        this.startTime = startTime;
        this.duration = duration;
        this.size = size;
        this.bitRate = bitRate;
    }
}
