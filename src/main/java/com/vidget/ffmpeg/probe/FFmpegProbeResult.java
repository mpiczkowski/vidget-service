package com.vidget.ffmpeg.probe;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.ToString;
import lombok.Value;

import java.util.List;

@Value
@Builder
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class FFmpegProbeResult {
    private final FFmpegFormat format;
    private final List<FFmpegStream> streams;

    @JsonCreator
    public FFmpegProbeResult(@JsonProperty("format") FFmpegFormat format,
                             @JsonProperty("streams") List<FFmpegStream> streams) {
        this.format = format;
        this.streams = streams;
    }
}
