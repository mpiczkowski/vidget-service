package com.vidget.ffmpeg.probe;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.ToString;
import lombok.Value;

@Value
@Builder
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class FFmpegStream {
    public enum CodecType {
        VIDEO,
        AUDIO,
    }

    private final int index;
    private final String codecName;
    private final String codecLongName;
    private final CodecType codecType;
    private final double duration;
    private final long bitRate;

    @JsonCreator
    public FFmpegStream(@JsonProperty("index") int index,
                        @JsonProperty("codec_name") String codecName,
                        @JsonProperty("codec_long_name") String codecLongName,
                        @JsonProperty("codec_type") CodecType codecType,
                        @JsonProperty("duration") double duration,
                        @JsonProperty("bit_rate") long bitRate) {
        this.index = index;
        this.codecName = codecName;
        this.codecLongName = codecLongName;
        this.codecType = codecType;
        this.duration = duration;
        this.bitRate = bitRate;
    }
}
