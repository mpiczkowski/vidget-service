package com.vidget.api;

import com.jayway.jsonpath.JsonPath;
import com.vidget.Application;
import com.vidget.testutil.TestUtils;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Files;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;

/**
 * Integration tests for video API
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class VideoControllerIT {

    private static final ClassPathResource VIDEO_FILE = new ClassPathResource("test-data/big_buck_bunny_720p_1mb.mp4");

    private static File largeFile;
    private static File notVideoFile;
    private static File emptyFile;

    @LocalServerPort
    private int port;

    private TestRestTemplate restTemplate = new TestRestTemplate();

    private HttpHeaders headers = new HttpHeaders();

    @BeforeClass
    public static void beforeClass() throws IOException {
        largeFile = File.createTempFile(UUID.randomUUID().toString(), ".mp4");
        notVideoFile = File.createTempFile(UUID.randomUUID().toString(), ".img");
        emptyFile = File.createTempFile(UUID.randomUUID().toString(), ".avi");
        TestUtils.createFileOfSizeMB(largeFile, 30);
        TestUtils.createFileOfSizeMB(notVideoFile, 1);
        TestUtils.createFileOfSizeMB(emptyFile, 0);
    }

    @AfterClass
    public static void afterClass() {
        Files.delete(largeFile);
        Files.delete(notVideoFile);
        Files.delete(emptyFile);
    }

    //TODO: cleanup uploaded files to not mess up test server
    @Test
    public void should_upload_and_get_metadata() {

        // upload
        ResponseEntity<String> response = uploadVideo(VIDEO_FILE);

        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
        String videoId = JsonPath.read(response.getBody(), "$.id");
        assertThat(videoId, not(isEmptyOrNullString()));

        // get metadata
        waitSeconds(1);// need to wait for async file processing

        ResponseEntity<String> getVideoResponse = getMetadata(videoId);

        assertThat(getVideoResponse.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void should_fail_on_uploading_when_no_file() {

        ResponseEntity<String> response = uploadVideo(null);

        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
        assertErrorMessage(response.getBody(), "File is empty");
    }

    @Test
    public void should_fail_on_uploading_when_empty_file() {

        ResponseEntity<String> response = uploadVideo(new FileSystemResource(emptyFile));

        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
        assertErrorMessage(response.getBody(), "File is empty");
    }

    @Test
    public void should_fail_on_uploading_when_not_video_file() {

        ResponseEntity<String> response = uploadVideo(new FileSystemResource(notVideoFile));

        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
        assertErrorMessage(response.getBody(), "File is not a video");
    }

    @Test
    public void should_fail_on_uploading_large_file() {

        ResponseEntity<String> response = uploadVideo(new FileSystemResource(largeFile));

        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
        assertErrorMessage(response.getBody(), "Maximum upload size exceeded");
    }

    @Test
    public void should_get_bad_request_when_id_is_not_uuid() {

        ResponseEntity<String> response = getMetadata("123");

        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void should_get_not_found_when_id_does_not_exist() {

        ResponseEntity<String> response = getMetadata("7e652e32-575c-45e2-b99e-0e0951cd12e4");

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    private ResponseEntity<String> getMetadata(String videoId) {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        return restTemplate.exchange(
                createURLWithPort("/v1/videos/" + videoId),
                HttpMethod.GET, entity, String.class);
    }

    private ResponseEntity<String> uploadVideo(Resource video) {
        LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        parameters.add("file", video);

        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);

        return restTemplate.exchange(
                createURLWithPort("/v1/videos"),
                HttpMethod.POST, entity, String.class, "");
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }


    /**
     * Wait for async operation to complete
     *
     * @param sec number of seconds to wait
     */
    private void waitSeconds(int sec) {
        try {
            Thread.sleep(Duration.ofSeconds(sec).toMillis());
        } catch (InterruptedException e) {
            log.error("Interrupted sleep", e);
        }
    }

    private void assertErrorMessage(String body, String expected) {
        String message = JsonPath.read(body, "$.message");
        assertThat(message, is(expected));
    }
}

