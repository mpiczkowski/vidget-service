package com.vidget.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.JacksonJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;
import com.vidget.api.validation.FileUploadValidator;
import com.vidget.domain.Stream;
import com.vidget.domain.Video;
import com.vidget.domain.services.VideoStoreService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.EnumSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static com.vidget.testutil.TestConstants.*;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Unit tests for video API
 */
@RunWith(SpringRunner.class)
@WebMvcTest(VideoController.class)
public class VideoControllerTest {

    private static final String VIDEOS_ENDPOINT = "/v1/videos/";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VideoStoreService videoStoreService;

    @MockBean
    private FileUploadValidator fileUploadValidator;


    @Before
    public void setup() {
        doNothing().when(fileUploadValidator).validate(any());

        Configuration.setDefaults(new Configuration.Defaults() {

            private final JsonProvider jsonProvider = new JacksonJsonProvider(objectMapper);
            private final MappingProvider mappingProvider = new JacksonMappingProvider(objectMapper);

            @Override
            public JsonProvider jsonProvider() {
                return jsonProvider;
            }

            @Override
            public MappingProvider mappingProvider() {
                return mappingProvider;
            }

            @Override
            public Set<Option> options() {
                return EnumSet.noneOf(Option.class);
            }
        });

    }

    @Test
    public void should_get_metadata_when_video_processed() throws Exception {
        UUID id = UUID.randomUUID();
        Video video = createVideo(id);
        when(videoStoreService.findById(id)).thenReturn(Optional.of(video));

        mockMvc.perform(get(VIDEOS_ENDPOINT + id))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.duration", is(DURATION)))
                .andExpect(jsonPath("$.size", is(SIZE)))
                .andExpect(jsonPath("$.audios[0].duration", is(AUDIO_DURATION)))
                .andExpect(jsonPath("$.audios[0].codecName", is(AUDIO_CODEC)))
                .andExpect(jsonPath("$.audios[0].bitRate", is(AUDIO_BIT_RATE)))
                .andExpect(jsonPath("$.videos[0].duration", is(VIDEO_DURATION)))
                .andExpect(jsonPath("$.videos[0].codecName", is(VIDEO_CODEC)))
                .andExpect(jsonPath("$.videos[0].bitRate", is(VIDEO_BIT_RATE)));
    }

    @Test
    public void should_get_processing_status_when_video_uploaded() throws Exception {
        UUID id = UUID.randomUUID();
        when(videoStoreService.binaryExists(id)).thenReturn(true);

        mockMvc.perform(get(VIDEOS_ENDPOINT + id))
                .andDo(print())
                .andExpect(status().isProcessing());
    }

    @Test
    public void should_get_not_found_status_when_no_video() throws Exception {
        UUID id = UUID.randomUUID();
        when(videoStoreService.binaryExists(id)).thenReturn(false);

        mockMvc.perform(get(VIDEOS_ENDPOINT + id))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    private Video createVideo(UUID id) {
        return Video.builder()
                .id(id)
                .duration(DURATION)
                .size(SIZE)
                .audios(ImmutableList.of(createAudioStream()))
                .videos(ImmutableList.of(createVideoStream()))
                .build();
    }

    private Stream createAudioStream() {
        return Stream.builder()
                .bitRate(AUDIO_BIT_RATE)
                .codecName(AUDIO_CODEC)
                .duration(AUDIO_DURATION)
                .build();
    }

    private Stream createVideoStream() {
        return Stream.builder()
                .bitRate(VIDEO_BIT_RATE)
                .codecName(VIDEO_CODEC)
                .duration(VIDEO_DURATION)
                .build();
    }

}