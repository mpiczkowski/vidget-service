package com.vidget.api.validation;

import com.vidget.api.validation.exceptions.InvalidFileTypeException;
import com.vidget.api.validation.exceptions.NoContentException;
import org.assertj.core.util.Arrays;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Supplier;

@RunWith(Parameterized.class)
public class FileUploadValidatorTest {

    private static final String CONTENT = "content";
    private static final String EMPTY = "";
    private static final String NAME = "file";
    private static final String CONTENT_TYPE = MediaType.APPLICATION_JSON_UTF8_VALUE;

    private final Optional<Exception> expectedException;
    private final MultipartFile multipartFile;

    private FileUploadValidator validator;

    public FileUploadValidatorTest(String originalFileName, String content, Supplier<Exception> exceptionSupplier) {
        this.multipartFile = new MockMultipartFile(NAME, originalFileName, CONTENT_TYPE, content.getBytes());
        this.expectedException = Optional.ofNullable(exceptionSupplier.get());
    }

    @Parameters
    public static Collection data() {
        return Arrays.asList(new Object[][]{
                {null, CONTENT, (Supplier<Exception>) InvalidFileTypeException::new},
                {EMPTY, CONTENT, (Supplier<Exception>) InvalidFileTypeException::new},
                {"noextension", CONTENT, (Supplier<Exception>) InvalidFileTypeException::new},
                {"name.mp3", CONTENT, (Supplier<Exception>) InvalidFileTypeException::new},
                {"name.mp4", EMPTY, (Supplier<Exception>) NoContentException::new},
                {"name.mp4", EMPTY, (Supplier<Exception>) NoContentException::new},
                {"name.avi", CONTENT, (Supplier<Exception>) () -> null},
                {"name..avi", CONTENT, (Supplier<Exception>) () -> null},
                {"name.avi", EMPTY, (Supplier<Exception>) NoContentException::new}
        });
    }

    @Rule
    public ExpectedException exceptionGrabber = ExpectedException.none();

    @Before
    public void setup() {
        validator = new FileUploadValidator("avi, mp4");
    }

    @Test
    public void should_validate() {

        expectedException.ifPresent(e -> exceptionGrabber.expect(e.getClass()));

        validator.validate(multipartFile);
    }

}