package com.vidget.domain.persistence;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.vidget.domain.Stream;
import com.vidget.domain.Video;
import com.vidget.domain.persistence.exceptions.DataAlreadyExistsException;
import com.vidget.domain.persistence.exceptions.DataStoreException;
import com.vidget.testutil.TestUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class FSVideoRepositoryTest {
    private static final UUID VIDEO_1 = UUID.randomUUID();
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final String DUMMY_CONTENT = "dummy content";

    private FSVideoRepository repository;
    private Path storeLocation;

    @Rule
    public ExpectedException exceptionGrabber = ExpectedException.none();


    @Before
    public void setup() throws IOException {
        storeLocation = Files.createTempDirectory("tempstore");
        Files.createFile(storeLocation.resolve(VIDEO_1.toString()));
        repository = new FSVideoRepository(storeLocation.toString(), objectMapper);
    }

    @After
    public void cleanup() throws IOException {
        TestUtils.cleanFolderRecursive(storeLocation);
    }

    @Test
    public void should_save_video() throws DataStoreException {
        UUID newVideoId = UUID.randomUUID();

        String savedPath = repository.saveVideo(newVideoId, DUMMY_CONTENT.getBytes());

        assertThat(savedPath, is(storeLocation.resolve(newVideoId.toString()).toString()));
    }

    @Test
    public void should_not_save_video_with_the_same_name() throws DataStoreException {
        exceptionGrabber.expect(DataAlreadyExistsException.class);
        exceptionGrabber.expectMessage(String.format("Video %s already exists", VIDEO_1));

        repository.saveVideo(VIDEO_1, DUMMY_CONTENT.getBytes());
    }

    @Test
    public void should_overwrite_metadata_json_if_present() throws DataStoreException, IOException {
        Video video = Video.builder()
                .id(VIDEO_1)
                .build();

        Path expectedMetadataPath = storeLocation.resolve(VIDEO_1 + ".json");

        repository.saveMetaData(video);

        assertTrue(expectedMetadataPath.toFile().exists());

        double duration = 11.1;
        Video videoUpdated = Video.builder()
                .id(VIDEO_1)
                .duration(duration)
                .build();

        repository.saveMetaData(videoUpdated);
        assertTrue(expectedMetadataPath.toFile().exists());

        Video deserialized = objectMapper.readValue(expectedMetadataPath.toFile(), Video.class);
        assertThat(deserialized.getDuration(), is(duration));
    }

    @Test
    public void should_not_find_video_by_id() throws DataStoreException {

        Optional<Video> video = repository.findById(UUID.randomUUID());

        assertThat(video, is(Optional.empty()));
    }

    @Test
    public void should_serialize_and_deserialize() throws DataStoreException {
        UUID newVideoId = UUID.randomUUID();
        double duration = 10.0;
        long size = 123;

        int indx = 1;
        long bitRate = 3;
        String codec = "codec name";
        Stream stream = Stream.builder()
                .bitRate(bitRate)
                .codecName(codec)
                .duration(duration)
                .index(indx)
                .build();

        Video video = Video.builder()
                .id(newVideoId)
                .duration(duration)
                .size(size)
                .audios(ImmutableList.of(stream))
                .videos(ImmutableList.of(stream))
                .build();

        Path expectedMetadataPath = storeLocation.resolve(newVideoId + ".json");

        repository.saveMetaData(video);

        assertTrue(expectedMetadataPath.toFile().exists());

        Optional<Video> foundVideo = repository.findById(newVideoId);

        assertTrue(foundVideo.isPresent());
        Video deserialized = foundVideo.get();
        assertThat(deserialized.getId(), is(newVideoId));
        assertThat(deserialized.getDuration(), is(duration));
        assertThat(deserialized.getSize(), is(size));

        validateStream(duration, indx, bitRate, codec, deserialized.getAudios());
        validateStream(duration, indx, bitRate, codec, deserialized.getVideos());
    }

    private void validateStream(double duration, int indx, long bitRate, String codec, List<Stream> streams) {
        assertThat(streams, is(notNullValue()));
        assertThat(streams.size(), is(1));
        Stream stream = streams.get(0);
        assertThat(stream.getBitRate(), is(bitRate));
        assertThat(stream.getCodecName(), is(codec));
        assertThat(stream.getDuration(), is(duration));
        assertThat(stream.getIndex(), is(indx));
    }

}