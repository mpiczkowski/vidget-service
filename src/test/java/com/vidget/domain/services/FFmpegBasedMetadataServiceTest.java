package com.vidget.domain.services;

import com.google.common.collect.ImmutableList;
import com.vidget.domain.Stream;
import com.vidget.domain.Video;
import com.vidget.domain.persistence.exceptions.DataStoreException;
import com.vidget.domain.services.exceptions.MetadataProcessingException;
import com.vidget.ffmpeg.probe.FFmpegFormat;
import com.vidget.ffmpeg.probe.FFmpegProbeResult;
import com.vidget.ffmpeg.probe.FFmpegStream;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static com.vidget.ffmpeg.probe.FFmpegStream.CodecType.AUDIO;
import static com.vidget.ffmpeg.probe.FFmpegStream.CodecType.VIDEO;
import static com.vidget.testutil.TestConstants.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FFmpegBasedMetadataServiceTest {

    private static final String DUMMY_EXCEPTION_MESSAGE = "something went wrong";
    private static final String DUMMY_PATH = "dummyPath";

    @Mock
    private FFmpegProcessor ffmpegProcessor;

    @Mock
    private VideoStoreService videoStoreService;

    @Rule
    public ExpectedException exceptionGrabber = ExpectedException.none();

    private FFmpegBasedMetadataService ffmpegBasedMetadataService;

    private UUID videoId;

    @Before
    public void setup() {
        videoId = UUID.randomUUID();
        ffmpegBasedMetadataService = new FFmpegBasedMetadataService(ffmpegProcessor, videoStoreService);
    }

    @Test
    public void should_handle_errors_from_ffmpeg_wrapper() throws IOException, MetadataProcessingException {
        IOException exception = new IOException(DUMMY_EXCEPTION_MESSAGE);
        doThrow(exception).when(ffmpegProcessor).probe(DUMMY_PATH);
        when(videoStoreService.getBinaryLocation(videoId)).thenReturn(DUMMY_PATH);

        exceptionGrabber.expect(MetadataProcessingException.class);
        exceptionGrabber.expectMessage(String.format("Cannot extract video metadata for %s", videoId));

        ffmpegBasedMetadataService.process(videoId);
    }

    @Test
    public void should_handle_errors_from_storage() throws IOException, MetadataProcessingException, DataStoreException {
        DataStoreException exception = new DataStoreException(DUMMY_EXCEPTION_MESSAGE);
        doThrow(exception).when(videoStoreService).save(any(Video.class));

        when(ffmpegProcessor.probe(DUMMY_PATH)).thenReturn(createProbeResult());
        when(videoStoreService.getBinaryLocation(videoId)).thenReturn(DUMMY_PATH);

        exceptionGrabber.expect(MetadataProcessingException.class);
        exceptionGrabber.expectMessage(String.format("Cannot save video metadata for %s", videoId));

        ffmpegBasedMetadataService.process(videoId);
    }

    @Test
    public void should_process_ok() throws IOException, MetadataProcessingException, DataStoreException {
        ArgumentCaptor<Video> argumentCaptor = ArgumentCaptor.forClass(Video.class);
        when(ffmpegProcessor.probe(DUMMY_PATH)).thenReturn(createProbeResult());
        when(videoStoreService.getBinaryLocation(videoId)).thenReturn(DUMMY_PATH);
        doNothing().when(videoStoreService).save(argumentCaptor.capture());

        ffmpegBasedMetadataService.process(videoId);

        Video result = argumentCaptor.getValue();
        assertThat(result, is(notNullValue()));
        assertThat(result.getDuration(), is(DURATION));
        assertThat(result.getSize(), is(SIZE));
        assertStream(result.getVideos(), VIDEO_DURATION, VIDEO_BIT_RATE, VIDEO_CODEC);
        assertStream(result.getAudios(), AUDIO_DURATION, AUDIO_BIT_RATE, AUDIO_CODEC);
    }

    private void assertStream(List<Stream> streams, double duration, long bitRate, String codec) {
        assertThat(streams, is(notNullValue()));
        assertThat(streams.size(), is(1));
        Stream stream = streams.get(0);
        assertThat(stream.getDuration(), is(duration));
        assertThat(stream.getBitRate(), is(bitRate));
        assertThat(stream.getCodecName(), is(codec));
    }

    private FFmpegProbeResult createProbeResult() {
        return FFmpegProbeResult.builder()
                .format(createFFmpegFormat())
                .streams(ImmutableList.of(createAudioStream(), createVideoStream()))
                .build();
    }

    private FFmpegStream createAudioStream() {
        return FFmpegStream.builder()
                .bitRate(AUDIO_BIT_RATE)
                .codecName(AUDIO_CODEC)
                .codecType(AUDIO)
                .duration(AUDIO_DURATION)
                .build();
    }

    private FFmpegStream createVideoStream() {
        return FFmpegStream.builder()
                .bitRate(VIDEO_BIT_RATE)
                .codecName(VIDEO_CODEC)
                .codecType(VIDEO)
                .duration(VIDEO_DURATION)
                .build();
    }

    private FFmpegFormat createFFmpegFormat() {
        return FFmpegFormat.builder()
                .size(SIZE)
                .duration(DURATION)
                .build();
    }

}