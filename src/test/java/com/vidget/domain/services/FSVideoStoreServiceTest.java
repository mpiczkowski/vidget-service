package com.vidget.domain.services;

import com.vidget.domain.Video;
import com.vidget.domain.events.VideoAddedEvent;
import com.vidget.domain.persistence.VideoRepository;
import com.vidget.domain.persistence.exceptions.DataStoreException;
import net.bytebuddy.utility.RandomString;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FSVideoStoreServiceTest {
    private static final String DUMMY_CONTENT = RandomString.make(4);
    private static final String DUMMY_PATH = RandomString.make(4);

    @Mock
    private VideoRepository videoRepository;

    @Mock
    private ApplicationEventPublisher eventPublisher;

    private FSVideoStoreService service;

    @Before
    public void before() {
        service = new FSVideoStoreService(videoRepository, eventPublisher);
    }

    @Test
    public void should_save_metadata() throws DataStoreException {
        Video videoToSave = Video.builder().build();

        service.save(videoToSave);

        verify(videoRepository).saveMetaData(videoToSave);
    }

    @Test
    public void should_save_binary() throws DataStoreException {
        ArgumentCaptor<UUID> idCaptor = ArgumentCaptor.forClass(UUID.class);
        ArgumentCaptor<byte[]> binaryCaptor = ArgumentCaptor.forClass(byte[].class);
        when(videoRepository.saveVideo(idCaptor.capture(), binaryCaptor.capture())).thenReturn(DUMMY_PATH);

        service.save(DUMMY_CONTENT.getBytes());

        assertThat(new String(binaryCaptor.getValue()), is(DUMMY_CONTENT));
    }


    @Test
    public void should_publish_event_after_saving_binary() throws DataStoreException {
        ArgumentCaptor<UUID> idCaptor = ArgumentCaptor.forClass(UUID.class);
        byte[] binary = DUMMY_CONTENT.getBytes();
        when(videoRepository.saveVideo(idCaptor.capture(), eq(binary))).thenReturn(DUMMY_PATH);

        ArgumentCaptor<VideoAddedEvent> eventCaptor = ArgumentCaptor.forClass(VideoAddedEvent.class);
        doNothing().when(eventPublisher).publishEvent(eventCaptor.capture());

        service.save(binary);

        UUID savedId = idCaptor.getValue();
        VideoAddedEvent publishedEvent = eventCaptor.getValue();
        assertThat(publishedEvent.getVideoId(), is(savedId));
    }
}