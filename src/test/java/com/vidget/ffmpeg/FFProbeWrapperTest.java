package com.vidget.ffmpeg;

import com.vidget.ffmpeg.probe.FFmpegFormat;
import com.vidget.ffmpeg.probe.FFmpegProbeResult;
import com.vidget.ffmpeg.probe.FFmpegStream;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class FFProbeWrapperTest {

    @Test
    public void should_get_video_attributes() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        String filePath = classLoader.getResource("test-data/big_buck_bunny_720p_1mb.mp4").getFile();

        FFmpegProbeResult result = FFProbeWrapper.run(filePath);

        assertThat(result, is(notNullValue()));
        FFmpegFormat format = result.getFormat();
        assertThat(format, is(notNullValue()));
        assertThat(format.getBitRate(), is(1589951L));
        assertThat(format.getSize(), is(1055728L));
        assertThat(format.getDuration(), is(5.312));
        assertThat(format.getStartTime(), is(0.0));
        assertThat(format.getFormatLongName(), is("QuickTime / MOV"));
        assertThat(format.getFormatName(), is("mov,mp4,m4a,3gp,3g2,mj2"));
        assertThat(format.getNbStreams(), is(2));
        assertThat(format.getFilename(), is(filePath));
        List<FFmpegStream> streams = result.getStreams();
        assertThat(streams.size(), is(2));
    }
}