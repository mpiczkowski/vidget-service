package com.vidget.testutil;

import net.bytebuddy.utility.RandomString;

import java.util.Random;

public class TestConstants {
    private TestConstants() {
    }

    private static final Random RANDOM = new Random();
    public static final double DURATION = RANDOM.nextDouble();
    public static final long SIZE = RANDOM.nextLong();
    public static final double VIDEO_DURATION = RANDOM.nextDouble();
    public static final long VIDEO_BIT_RATE = RANDOM.nextLong();
    public static final String VIDEO_CODEC = RandomString.make(4);
    public static final double AUDIO_DURATION = RANDOM.nextDouble();
    public static final long AUDIO_BIT_RATE = RANDOM.nextLong();
    public static final String AUDIO_CODEC = RandomString.make(4);

}
