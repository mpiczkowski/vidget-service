package com.vidget.testutil;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

public class TestUtils {
    private TestUtils() {
    }

    public static void cleanFolderRecursive(Path folderPath) throws IOException {
        File folder = folderPath.toFile();
        if (folder.exists() && folder.isDirectory()) {
            Arrays.asList(folder.listFiles()).forEach(File::delete);
        }
        Files.delete(folderPath);
    }

    public static void createFileOfSizeMB(File file, long sizeMB) throws IOException {
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw")) {
            randomAccessFile.setLength(1024 * 1024 * sizeMB);
        }
    }

}
